/*
 * Copyright (c) 2015 Davide Compagnin
 * Licensed under the MIT license.
 */

#include <sys/time.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <assert.h>

/* include the main liblitmus header */
#include <litmus.h>
#include <common.h>

/* Catch errors.
 */
#define CALL( exp ) do { \
		int ret; \
		ret = exp; \
		if (ret != 0) \
			fprintf(stderr, "%s failed: %m\n", #exp);\
		else \
			fprintf(stderr, "%s ok.\n", #exp); \
	} while (0)

#define MB_SIZE 1024*1024
#define BUFFER_SIZE 16*MB_SIZE

extern void enter_np(void);

extern void exit_np(void);

static void usage(char *error) {
	fprintf(stderr, "Error: %s\n", error);
	fprintf(stderr,
		"Usage:\n"
		"	rt_spin [COMMON-OPTS] FILE PERIOD\n"
		"\n"
		"COMMON-OPTS = [-w][-n]\n"
		"              [-p PARTITION/CLUSTER]\n"
		"				[-j jobs]\n"
		"              [-d DEBUG]\n"
		"			   [-b BUFFER_SIZE]"
		"\n");
	exit(EXIT_FAILURE);
}

/*
 * returns the character that made processing stop, newline or EOF
 */
static int skip_to_next_line(FILE *fstream)
{
	int ch;
	for (ch = fgetc(fstream); ch != EOF && ch != '\n'; ch = fgetc(fstream));
	return ch;
}

static void skip_comments(FILE *fstream)
{
	int ch;
	for (ch = fgetc(fstream); ch == '#'; ch = fgetc(fstream))
		skip_to_next_line(fstream);
	ungetc(ch, fstream);
}

static void get_access_sequence(const char *file, int *sequence_length, long **sequence)
{
	FILE *fstream;
	int  cur_value, ch;
	
	*sequence_length = 0;
	fstream = fopen(file, "r");
	if (!fstream)
		bail_out("could not open execution time file");
		
	/* figure out the number of jobs */
	do {
		skip_comments(fstream);
		ch = skip_to_next_line(fstream);
		if (ch != EOF)
			++(*sequence_length);
	} while (ch != EOF);
	if (-1 == fseek(fstream, 0L, SEEK_SET))
		bail_out("rewinding file failed");

	/* allocate space for the sequence */
	*sequence = calloc(*sequence_length, sizeof(*sequence));
	if (!*sequence)
		bail_out("couldn't allocate memory");

	for (cur_value = 0; cur_value < *sequence_length && !feof(fstream); ++cur_value) {

		skip_comments(fstream);
		
		/* get the desired iteration value */
		if (1 != fscanf(fstream, "%ld", (*sequence)+cur_value)) {
			fprintf(stderr, "invalid value %d\n", cur_value);
			exit(EXIT_FAILURE);
		}
		
		skip_to_next_line(fstream);
	}
	
	assert(cur_value == *sequence_length);
	fclose(fstream);
}

static int check_for_buffer_overflow(long *sequence, int sequence_length, int buffer_size) 
{
	int i;
	for (i = 1; i < sequence_length; i = i + 2)
		if (sequence[i] > buffer_size)
			return 1;
	return 0;
}

static void cpu_loop(int iterations)
{
	while(iterations)
		__asm__ volatile("dec %0"
         : "=r" (iterations)
         : "r" (iterations));
}

static inline void memory_read_128(char *addr) {
	register int temp;
	__asm__ volatile(
		"mov (%1), %0\n\t"
		"mov -1(%1), %0\n\t"
		"mov -2(%1), %0\n\t"
		"mov -3(%1), %0\n\t"
		"mov -4(%1), %0\n\t"
		"mov -5(%1), %0\n\t"
		"mov -6(%1), %0\n\t"
		"mov -7(%1), %0\n\t"
		"mov -8(%1), %0\n\t"
		"mov -9(%1), %0\n\t"
		"mov -10(%1), %0\n\t"
		"mov -11(%1), %0\n\t"
		"mov -12(%1), %0\n\t"
		"mov -13(%1), %0\n\t"
		"mov -14(%1), %0\n\t"
		"mov -15(%1), %0\n\t"
		"mov -16(%1), %0\n\t"
		"mov -17(%1), %0\n\t"
		"mov -18(%1), %0\n\t"
		"mov -19(%1), %0\n\t"
		"mov -20(%1), %0\n\t"
		"mov -21(%1), %0\n\t"
		"mov -22(%1), %0\n\t"
		"mov -23(%1), %0\n\t"
		"mov -24(%1), %0\n\t"
		"mov -25(%1), %0\n\t"
		"mov -26(%1), %0\n\t"
		"mov -27(%1), %0\n\t"
		"mov -28(%1), %0\n\t"
		"mov -29(%1), %0\n\t"
		"mov -30(%1), %0\n\t"
		"mov -31(%1), %0\n\t"
		"mov -32(%1), %0\n\t"
		"mov -33(%1), %0\n\t"
		"mov -34(%1), %0\n\t"
		"mov -35(%1), %0\n\t"
		"mov -36(%1), %0\n\t"
		"mov -37(%1), %0\n\t"
		"mov -38(%1), %0\n\t"
		"mov -39(%1), %0\n\t"
		"mov -40(%1), %0\n\t"
		"mov -41(%1), %0\n\t"
		"mov -42(%1), %0\n\t"
		"mov -43(%1), %0\n\t"
		"mov -44(%1), %0\n\t"
		"mov -45(%1), %0\n\t"
		"mov -46(%1), %0\n\t"
		"mov -47(%1), %0\n\t"
		"mov -48(%1), %0\n\t"
		"mov -49(%1), %0\n\t"
		"mov -50(%1), %0\n\t"
		"mov -51(%1), %0\n\t"
		"mov -52(%1), %0\n\t"
		"mov -53(%1), %0\n\t"
		"mov -54(%1), %0\n\t"
		"mov -55(%1), %0\n\t"
		"mov -56(%1), %0\n\t"
		"mov -57(%1), %0\n\t"
		"mov -58(%1), %0\n\t"
		"mov -59(%1), %0\n\t"
		"mov -60(%1), %0\n\t"
		"mov -61(%1), %0\n\t"
		"mov -62(%1), %0\n\t"
		"mov -63(%1), %0\n\t"
		"mov -64(%1), %0\n\t"
		"mov -65(%1), %0\n\t"
		"mov -66(%1), %0\n\t"
		"mov -67(%1), %0\n\t"
		"mov -68(%1), %0\n\t"
		"mov -69(%1), %0\n\t"
		"mov -70(%1), %0\n\t"
		"mov -71(%1), %0\n\t"
		"mov -72(%1), %0\n\t"
		"mov -73(%1), %0\n\t"
		"mov -74(%1), %0\n\t"
		"mov -75(%1), %0\n\t"
		"mov -76(%1), %0\n\t"
		"mov -77(%1), %0\n\t"
		"mov -78(%1), %0\n\t"
		"mov -79(%1), %0\n\t"
		"mov -80(%1), %0\n\t"
		"mov -81(%1), %0\n\t"
		"mov -82(%1), %0\n\t"
		"mov -83(%1), %0\n\t"
		"mov -84(%1), %0\n\t"
		"mov -85(%1), %0\n\t"
		"mov -86(%1), %0\n\t"
		"mov -87(%1), %0\n\t"
		"mov -88(%1), %0\n\t"
		"mov -89(%1), %0\n\t"
		"mov -90(%1), %0\n\t"
		"mov -91(%1), %0\n\t"
		"mov -92(%1), %0\n\t"
		"mov -93(%1), %0\n\t"
		"mov -94(%1), %0\n\t"
		"mov -95(%1), %0\n\t"
		"mov -96(%1), %0\n\t"
		"mov -97(%1), %0\n\t"
		"mov -98(%1), %0\n\t"
		"mov -99(%1), %0\n\t"
		"mov -100(%1), %0\n\t"
		"mov -101(%1), %0\n\t"
		"mov -102(%1), %0\n\t"
		"mov -103(%1), %0\n\t"
		"mov -104(%1), %0\n\t"
		"mov -105(%1), %0\n\t"
		"mov -106(%1), %0\n\t"
		"mov -107(%1), %0\n\t"
		"mov -108(%1), %0\n\t"
		"mov -109(%1), %0\n\t"
		"mov -110(%1), %0\n\t"
		"mov -111(%1), %0\n\t"
		"mov -112(%1), %0\n\t"
		"mov -113(%1), %0\n\t"
		"mov -114(%1), %0\n\t"
		"mov -115(%1), %0\n\t"
		"mov -116(%1), %0\n\t"
		"mov -117(%1), %0\n\t"
		"mov -118(%1), %0\n\t"
		"mov -119(%1), %0\n\t"
		"mov -120(%1), %0\n\t"
		"mov -121(%1), %0\n\t"
		"mov -122(%1), %0\n\t"
		"mov -123(%1), %0\n\t"
		"mov -124(%1), %0\n\t"
		"mov -125(%1), %0\n\t"
		"mov -126(%1), %0\n\t"
		"mov -127(%1), %0"
         : "=r" (temp)
         : "r" (addr));
}

static inline void memory_read_cache_line(char *addr) {
	register int temp;
	__asm__ volatile(
		"mov (%1), %0\n\t"
		"mov -64(%1), %0\n\t"
		"mov -128(%1), %0\n\t"
		"mov -192(%1), %0\n\t"
		"mov -256(%1), %0\n\t"
		"mov -320(%1), %0\n\t"
		"mov -384(%1), %0\n\t"
		"mov -448(%1), %0\n\t"
		"mov -512(%1), %0\n\t"
		"mov -576(%1), %0\n\t"
		"mov -640(%1), %0\n\t"
		"mov -704(%1), %0\n\t"
		"mov -768(%1), %0\n\t"
		"mov -832(%1), %0\n\t"
		"mov -896(%1), %0\n\t"
		"mov -960(%1), %0\n\t"
		"mov -1024(%1), %0"
         : "=r" (temp)
         : "r" (addr));
}

static void memory_loop(int acc_bytes, char *buffer)
{
	register char* addr;
	while(acc_bytes) {
		addr = buffer+acc_bytes-1;
		/*__asm__ volatile(
			"mov %1, %0\n\t" //read
			"mov %0, %1"		//write
         : "=r" (temp)
         : "m" (*addr));*/
        memory_read_128(addr);
        acc_bytes-=128;
        //memory_read_cache_line(addr);
		//acc_bytes-=1024;
	}
}

static void loop_sequence(long *sequence, int sequence_length, char *buffer)
{
	register int block = 0;
	while(block < sequence_length) {
		cpu_loop(sequence[block]);
		block++;
		if(block < sequence_length) {
			memory_loop(sequence[block], buffer);
			block++;	
		}
	}
}

static int job(int *jobs, long *sequence, int sequence_length, char *buffer)
{
	if (*jobs < 1)
		return 1;
	loop_sequence(sequence, sequence_length, buffer);
	(*jobs)--;
	return 0;
}

static void debug_execution_time(long *sequence, int sequence_length, char* buffer)
{
	double start, end;
	
	while (1) {
		start = wctime();
		loop_sequence(sequence, sequence_length, buffer);
		end = wctime();
		printf("executed for %10.8fs\n", end - start);
	}
}

/* The main() does a couple of things: 
 * 	1) parse command line parameters, etc.
 *	2) Setup work environment.
 *	3) Setup real-time parameters.
 *	4) Transition to real-time mode.
 *	5) Invoke periodic or sporadic jobs.
 *	6) Transition to background mode.
 *	7) Clean up and exit.
 */
#define OPTSTR "p:j:wdb:n"
int main(int argc, char** argv)
{
	
	int do_exit;
	int migrate = 0;
	int cluster = 0;
	int jobs = 1;
	int wait = 0;
	int debug = 0;
	lt_t wcet;
	lt_t period;
	double wcet_ms = 1;
	double period_ms;
	int opt;
	const char *file = NULL;
	long *sequence = NULL;
	int sequence_length = 0;
	char *buffer = NULL;
	int buffer_size = BUFFER_SIZE;
	int np = 0; 
	struct rt_task param;
	
	/* The task is in background mode upon startup. */

	/*****
	 * 1) Command line paramter parsing would be done here.
	 */

	while ((opt = getopt(argc, argv, OPTSTR)) != -1) {
		switch (opt) {
		case 'w':
			wait = 1;
			break;
		case 'p':
			cluster = atoi(optarg);
			migrate = 1;
			break;
		case 'j':
			jobs = atoi(optarg);
			break;
		case 'd':
			debug = 1;
			break;
		case 'n':
			np = 1;
			break;
		case 'b':
			buffer_size = atoi(optarg);
			break;
		case '?':
		default:
			usage("Bad argument.");
			break;
		}
	}
	
	if (argc - optind < 2)
		usage("Arguments missing.");
	
	buffer = malloc(buffer_size*sizeof(char));	
	if (!buffer)
		bail_out("couldn't allocate memory");
	
	srand(getpid());
	
	file = argv[optind + 0];
	
	get_access_sequence(file, &sequence_length, &sequence);
	assert(sequence_length > 0);
	
	if (check_for_buffer_overflow(sequence, sequence_length, buffer_size))
		bail_out("buffer overflow");
	
	if (debug)
		debug_execution_time(sequence, sequence_length, buffer);
	
	period_ms   = atof(argv[optind + 1]);
	wcet   = ms2ns(wcet_ms);
	period = ms2ns(period_ms);
	
	
	/*****
	 * 3) Setup real-time parameters. 
	 *    In this example, we create a sporadic task that does not specify a 
	 *    target partition (and thus is intended to run under global scheduling). 
	 *    If this were to execute under a partitioned scheduler, it would be assigned
	 *    to the first partition (since partitioning is performed offline).
	 */
	
	CALL( be_migrate_to_domain(cluster) );
	
	/* Setup task parameters */
	init_rt_task_param(&param);
	
	param.exec_cost = wcet;
	param.period = period;
	//param.relative_deadline = ms2ns(period_ms);

	/* What to do in the case of budget overruns? */
	param.budget_policy = NO_ENFORCEMENT;

	/* The task class parameter is ignored by most plugins. */
	if (np)
		param.cls = RT_CLASS_HARD_NP;
	else 
		param.cls = RT_CLASS_HARD;
	
	/* The priority parameter is only used by fixed-priority plugins. */
	param.priority = LITMUS_LOWEST_PRIORITY;
	
	if (migrate)
		param.cpu = domain_to_first_cpu(cluster);
	
	CALL( set_rt_task_param(gettid(), &param) );

	CALL( init_litmus() );
	
	/*****
	 * 4) Transition to real-time mode.
	 */
	CALL( task_mode(LITMUS_RT_TASK) );

	/* The task is now executing as a real-time task if the call didn't fail. 
	 */

	if (wait)
		CALL( wait_for_ts_release() );
			
	/*****
	 * 5) Invoke real-time jobs.
	 */
	do {
		/* Wait until the next job is released. */
		sleep_next_period();
		/* Invoke job. */
		do_exit = job(&jobs, sequence, sequence_length, buffer);
	} while (!do_exit);
	/*****
	 * 6) Transition to background mode.
	 */
	CALL( task_mode(BACKGROUND_TASK) );

	/***** 
	 * 7) Clean up, maybe print results and stats, and exit.
	 */
	return 0;
}
